﻿// SPDX-License-Identifier: GPL-3.0-only

using System.IO.Ports;
using Imperfect.Devices.Virtual.FioPulser.Command;
using log4net;

namespace Imperfect.Devices.Virtual.FioPulser
{
    internal class FioPulser
    {
        private static readonly ILog ReceivedLogger = LogManager.GetLogger("Received");
        
        private readonly SerialPort serialPort;
        private readonly CommandSelector commandSelector;
        
        public FioPulser(SerialPort serialPort)
        {
            this.serialPort = serialPort;
            commandSelector = new CommandSelector(serialPort);
            
            serialPort.DataReceived += OnDateReceivedOnSerialPort;
            serialPort.ErrorReceived += OnErrorReceivedOnSerialPort;
        }

        private void OnDateReceivedOnSerialPort(object _, SerialDataReceivedEventArgs args)
        {
            string command;
            lock (serialPort)
            {
                command = serialPort.ReadLine();
            }
            ReceivedLogger.Info(command);
            commandSelector.SelectCommand(command).Run();
        }
        
        private void OnErrorReceivedOnSerialPort(object _, SerialErrorReceivedEventArgs e)
        {
            ReceivedLogger.Error(e);
        }

    }
}
