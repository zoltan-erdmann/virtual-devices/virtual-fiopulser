﻿// SPDX-License-Identifier: GPL-3.0-only

using System.IO.Ports;
using System.Threading;
using Imperfect.Devices.Virtual.FioPulser.Command.Pulse;
using log4net;

namespace Imperfect.Devices.Virtual.FioPulser.Command
{
    internal class PulseCommand : Command
    {
        private static readonly ILog SendLogger = LogManager.GetLogger("Senging");
        
        private readonly SerialPort serialPort;
        private readonly PulseArguments arguments;

        public PulseCommand(SerialPort serialPort, PulseArguments args)
        {
            this.serialPort = serialPort;
            arguments = args;
        }

        public void Run()
        {
            Thread.Sleep(arguments.PeriodCount * arguments.PeriodLength);
            var message = $"c {arguments.Channel}";
            SendLogger.Info(message);
            serialPort.WriteLine(message);
        }
    }
}