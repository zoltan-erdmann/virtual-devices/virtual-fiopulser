﻿// SPDX-License-Identifier: GPL-3.0-only

using System.Text.RegularExpressions;

namespace Imperfect.Devices.Virtual.FioPulser.Command.Pulse
{
    internal class PulseArguments
    {
        public int Channel { get; private init; }
        public int PeriodCount { get; private init; }
        public int PeriodLength { get; private init; }

        public static PulseArguments Parse(string message)
        {
            var channel = 0;
            var periodCount = 0;
            var periodLength = 0;

            const string pattern = @"p ([1..4]+) (\d+) (\d+)";
            var regex = new Regex(pattern, RegexOptions.IgnoreCase);
            var match = regex.Match(message);
            if (match.Success && match.Groups.Count > 3)
            {
                int.TryParse(match.Groups[1].Value, out channel);
                int.TryParse(match.Groups[2].Value, out periodCount);
                int.TryParse(match.Groups[3].Value, out periodLength);
            }
            return new PulseArguments()
            {
                Channel = channel,
                PeriodCount = periodCount,
                PeriodLength = periodLength
            };
        }
    }
}