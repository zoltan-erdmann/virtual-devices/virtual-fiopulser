﻿// SPDX-License-Identifier: GPL-3.0-only

namespace Imperfect.Devices.Virtual.FioPulser.Command
{
    internal interface Command
    {
        void Run();
    }
}