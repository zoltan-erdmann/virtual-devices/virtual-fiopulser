﻿// SPDX-License-Identifier: GPL-3.0-only

using System.IO.Ports;
using Imperfect.Devices.Virtual.FioPulser.Properties;
using log4net;

namespace Imperfect.Devices.Virtual.FioPulser.Command
{
    internal class VersionCommand : Command
    {
        private static readonly ILog SendLogger = LogManager.GetLogger("Senging");
        
        private readonly SerialPort serialPort;
        
        public VersionCommand(SerialPort serialPort)
        {
            this.serialPort = serialPort;
        }
        
        public void Run()
        {
            LoggedWriteLine(Constants.Version);
            LoggedWriteLine(Constants.SerialNumber);
            LoggedWriteLine(Constants.ReleaseDate);
            LoggedWriteLine(Constants.Identifier);
        }

        private void LoggedWriteLine(string message)
        {
            SendLogger.Info(message);
            serialPort.WriteLine(message);
        }
    }
}