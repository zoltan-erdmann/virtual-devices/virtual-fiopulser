﻿// SPDX-License-Identifier: GPL-3.0-only

using System.IO.Ports;
using Imperfect.Devices.Virtual.FioPulser.Command.Pulse;

namespace Imperfect.Devices.Virtual.FioPulser.Command
{
    internal class CommandSelector
    {
        private readonly SerialPort serialPort;
        
        public CommandSelector(SerialPort serialPort)
        {
            this.serialPort = serialPort;
        }
        
        public Command SelectCommand(string message)
        {
            Command command;
            if (message.StartsWith("p"))
            {
                command = new PulseCommand(serialPort, PulseArguments.Parse(message));
            }
            else if (message.Equals("v"))
            {
                command = new VersionCommand(serialPort);
            }
            else
            {
                command = new UnknownCommand(serialPort);
            }

            return command;
        }
    }
}