﻿// SPDX-License-Identifier: GPL-3.0-only

using System.IO.Ports;
using log4net;

namespace Imperfect.Devices.Virtual.FioPulser.Command
{
    internal class UnknownCommand : Command
    {
        private static readonly ILog SendLogger = LogManager.GetLogger("Senging");
        
        private readonly SerialPort serialPort;
        
        public UnknownCommand(SerialPort serialPort)
        {
            this.serialPort = serialPort;
        }
        
        public void Run()
        {
            const string message = "unknown";
            SendLogger.Info(message);
            serialPort.WriteLine(message);
        }
    }
}