﻿// SPDX-License-Identifier: GPL-3.0-only

namespace Imperfect.Devices.Virtual.FioPulser.Properties
{
    internal static class Constants
    {
        public const string Version = "Version 1.02 00.01";
        public const string SerialNumber = "Serial number 12002";
        public const string ReleaseDate = "created on 2013. 01. 10. 22:55:11";
        public const string Identifier = "Device identifier 100";
    }
}