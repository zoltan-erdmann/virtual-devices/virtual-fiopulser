﻿// SPDX-License-Identifier: GPL-3.0-only

using System;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using Imperfect.Devices.Virtual.FioPulser.Properties;
using log4net;
using log4net.Config;

namespace Imperfect.Devices.Virtual.FioPulser
{
    class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));
        
        static void Main(string[] args)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            if (args.Length < 1)
            {
                PrintHelp();
            }
            else
            {
                StartFioPulser(args[0]);
            }
        }

        private static void PrintHelp()
        {
            Console.Out.WriteLine($"Usage: {Assembly.GetExecutingAssembly().GetName().Name} <COM Port>");
            Console.Out.WriteLine();
        }

        private static void StartFioPulser(string serialPortName)
        {
            using var serialPort = new SerialPort(serialPortName)
            {
                Parity = Parity.None,
                DataBits = 8,
                StopBits = StopBits.One,
                BaudRate = 9600,
                Handshake = Handshake.None,
                NewLine = "\r"
            };
            serialPort.Open();
            Logger.Info(Constants.Version);
            Logger.Info(Constants.SerialNumber);
            Logger.Info(Constants.ReleaseDate);
            Logger.Info(Constants.Identifier);
            Logger.Info($"Listening on {serialPortName}");

            var _ = new FioPulser(serialPort);
            Logger.Info("Hit enter to exit.");
            Console.ReadKey();
        }
    }
}